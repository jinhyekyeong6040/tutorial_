﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PropertyExam_2
{
    class Program
    {
        static void Main(string[] args)
        {
            var namecard = new { Name = "진혜경", Age = 22 };
            Console.WriteLine("이름 : {0}, 나이 : {1}", namecard.Name, namecard.Age);

            var complex = new { Real = "legs", Imaginary = "Money" };
            Console.WriteLine("Real : {0}, Imaginary : {1}", complex.Real, complex.Imaginary);
            Console.WriteLine("변경 사항 커밋 되나용");
        }
    }
}
